import React from 'react'
import { Input } from 'antd'
import { SearchOutlined } from '@ant-design/icons'
import _debounce from 'lodash.debounce'
import './searchComponent.css'

const InputCore = props => {
  const placeholder = props.placeholder || ''
  const onSearchChange = event => {
    const key = event.target.value
    console.log(key)
    processSearch(key)
  }

  const processSearch = _debounce(key => {
    return props.handleSearch(key)
  }, 500)
  
  return(
    <Input 
      className='search_Component'
      placeholder={placeholder} 
      onChange={(event) => onSearchChange(event)}
      prefix={<SearchOutlined />}
    >
    </Input>
  )
}

export const UISearch = InputCore

