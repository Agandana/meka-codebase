import React from "react";
import { Layout, Typography, Menu } from 'antd'
import { withRouter, NavLink } from "react-router-dom";
import './header.css'
const { Header } = Layout
const { Text } = Typography
const HeaderComponent = () => {
  return (
    <Header id="component_id">
      {/* <NavLink to='/dashboard'> */}
        <Text className='header_container'> Awesome!</Text>
      {/* </NavLink> */}
      <Menu theme='dark' mode='horizontal' defaultSelectedKeys='1'>
        <Menu.Item key='1'>
          <NavLink to='/dashboard'>
            Home
          </NavLink>
        </Menu.Item>
        <Menu.Item key='2'>
          <NavLink to='/example-form'>
            Example Form
          </NavLink>
        </Menu.Item>
        <Menu.Item key='3'>
          <NavLink to='/example-chat'>
            Example Chat
          </NavLink>
        </Menu.Item>
        <Menu.Item key='4'>
          <NavLink to='/example-widget'>
            Example widget
          </NavLink>
        </Menu.Item>
      </Menu>
    </Header>
  )
};

export default withRouter(HeaderComponent);
