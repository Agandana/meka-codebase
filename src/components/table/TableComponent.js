import React from 'react'
import { Table, Row, Col } from 'antd'

const TableCore = (props) => {
  console.log(props)
  const pagination = {
    defaultCurrent: 1,
    pageSize: 10,
    showSizeChanger: false
  }
  const defaultColums = [
    {
      title: 'id',
      dataIndex: 'id',
      key: 'id'
    },
    {
      title: 'name',
      dataIndex: 'name',
      key: 'name'
    },
  ]
  const dataTable = props.dataTable || []
  const className = props.className || ''
  const columns = props.columns || defaultColums
  const loading = props.loading === 'loading' ? true : false
  return(
    <Row>
      <Col span={24}>
        <Table 
          // className={className} 
          dataSource={dataTable}
          columns={columns}
          loading={loading}
          pagination= {pagination}
          // unique id per ROW
          rowKey={record => record.id}
        />
      </Col>
    </Row>
  )
}

export const UItable = TableCore