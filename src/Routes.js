import React, {Component} from 'react'
import { Route, Redirect, BrowserRouter} from 'react-router-dom'
import HeaderComponent from './components/header/HeaderComponent'
import Home from './module/Home/home'
import LandingPage from './module/LandingPage/landingPage'
import ExampleForm from './module/ExampleForm/exampleForm'
import ExampleChat from './module/ExampleChat'
import ExampleWidget from './module/ExampleWidget'

function PrivateRoute ({ ...props }) {
  if (
    sessionStorage.getItem("login")
  )
    return <Route { ...props } />
  else
    sessionStorage.clear();
    return <Redirect to="/" />
}

export default class Routes extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLogin: false
    }
  }

  componentDidMount() {
    if(sessionStorage.getItem('login')) {
      this.setState({
        isLogin: true
      })
    }else {
      this.setState({
        isLogin: false
      })
    }
  }

  onLogin = () => {
    this.setState({
      isLogin: true
    })
  }
  
  render() {
    const { isLogin } = this.state
    return(
      <BrowserRouter>
          {isLogin ? 
            <HeaderComponent />
            : 
            <></>
          }
          <Route exact path="/" render={props => <LandingPage onLogin={() => this.onLogin()}  {...props}/>} />
          <PrivateRoute path="/dashboard" render={ props => <Home {...props} />}  />
          <PrivateRoute path="/example-form" render={ props => <ExampleForm {...props} />}  />
          <PrivateRoute path="/example-chat" render={ props => <ExampleChat {...props} />} />
          <PrivateRoute path="/example-widget" render={ props => <ExampleWidget {...props} />} />
      </BrowserRouter>
    )
  }
}