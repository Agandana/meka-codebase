export const CONFIG_INITIAL_STATE = {
    BASE_URL: "https://my-json-server.typicode.com/",
    BASE_URL_LOGIN: "https://my-json-server.typicode.com/",
    BASE_URL_NOAUTH: "https://my-json-server.typicode.com/",
    COPYRIGHT: '20200716',
    VERSION: '0.0.2',
    TIMEOUT: 6000,
}
  