import { ApiAiClient } from 'api-ai-javascript'
const accessToken = '9119dea946d345a1b345d562ef1ab26c'
const client = new ApiAiClient({accessToken})


const getChatBot = (message) => {
  return client.textRequest(message).then(response => {
    return {
      data: response.result.fulfillment.speech,
      status:'success',
      error: ''
    }
  }).catch(error => {
    return {
      status: 'error',
      error,
      data: []
    }
  })
}
  // return get(`${BASE_URL}afifbasya/reactjs-redux/users`, {
  //   timeout: TIMEOUT
  // }).then(response => {
  //   return {
  //     data: response.data,
  //     status:'success',
  //     error: ''
  //   }
  // }).catch(error => {
  //   return {
  //     status: 'error',
  //     error,
  //     data: []
  //   }
  // })

export default {
  getChatBot
}