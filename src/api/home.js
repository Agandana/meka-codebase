import { get } from 'axios'
import { CONFIG_INITIAL_STATE } from '../services/config'

const { BASE_URL, TIMEOUT } = CONFIG_INITIAL_STATE

const getHome = () => {
  return get(`${BASE_URL}afifbasya/reactjs-redux/users`, {
    timeout: TIMEOUT
  }).then(response => {
    return {
      data: response.data,
      status:'success',
      error: ''
    }
  }).catch(error => {
    return {
      status: 'error',
      error,
      data: []
    }
  })
}

export default {
  getHome
}