import React, { Component } from 'react'
import { connect } from 'react-redux'
import getHome from '../../actions/home'
import { Row, Col, Typography } from 'antd'
import { UItable, UISearch, UISelect } from '../../components'
import './home.css'

const { Title } = Typography

class Home extends Component {
  componentDidMount() {
    this.props.handleDataHome()
  }

  handleSearch = value => {
    console.log('container value serach :', value)
  }

  handleSelect = value => {
    console.log('container value filter :', value)
  }
  
  render() {
    const LOV = [
      {
        id: '1',
        value: 'Pekalongan'
      },
      {
        id: '2',
        value: 'Jakarta'
      },
      {
        id: '3',
        value: 'Palu'
      },
      {
        id: '4',
        value: 'Ngawi'
      }
    ]
    const columnsTable = [
      {
        title: 'id',
        dataIndex: 'id',
        key: 'id'
      },
      {
        title: 'nama',
        dataIndex: 'nama',
        key: 'nama'
      },
      {
        title: 'alamat',
        dataIndex: 'alamat',
        key: 'alamat'
      },
      {
        title: 'nohp',
        dataIndex: 'nohp',
        key: 'nohp'
      },
      {
        title: 'umur',
        dataIndex: 'umur',
        key: 'umur'
      },
    ]
    const { dataHome } = this.props
    return (
      <div className="home-body">
        <Row justify='center'>
          <Col span={20}>
            <Row gutter={16} className="home_title">
              <Col span={8}>
                <Title>Example Table</Title>
              </Col>
              <Col span={6} flex='1' offset={4} className='search_component'>
                <UISelect 
                  dataLOV={LOV}
                  placeholder='Filter by city'
                  handleSelect={(value => this.handleSelect(value))}
                />
              </Col>
              <Col span={6}  className='search_component'>
                <UISearch 
                  handleSearch={value => this.handleSearch(value)} 
                  placeholder='Search user'
                />
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <UItable 
                  dataTable={dataHome.data}
                  columns={columnsTable}
                  loading={dataHome.status}
                  />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return{
    handleDataHome: () => dispatch(getHome.getDataHome())
  }
}

const mapStateToProps = (state) => {
  return {
    dataHome: state.home,
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home)