import React, { Component } from 'react'
import { connect } from 'react-redux'
// import { UIChat } from '../../components'
import getChatbot from '../../actions/chatbot'
import { Row, Col, Typography, Input } from 'antd'
import { SendOutlined } from '@ant-design/icons'
import './chat.css'

const {Title, Text} = Typography

class ExampleChat extends Component {
  constructor(props){
    super(props)
    this.state = {
      inputKey: '',
      chatHistory: []
    }
  }

  componentDidMount() {
    this.handleHitChatBot('Hi')
  }

  handleHitChatBot = (val) => {
    this.props.handleChatBot(val)
    // if (val !== 'hi') {
      this.handleSaveChat(val, 'You')
    // }
  }

  handleKeyPrees = (val) => {
    const key = val.target.value
    this.setState({
      inputKey: key
    })
  }

  handleOnClick = () => {
    let { inputKey } = this.state
    this.handleHitChatBot(inputKey)
    this.setState({
      inputKey: ''
    })
  }
  componentDidUpdate(prevPros) {
    if (prevPros.dataChat.data !== this.props.dataChat.data) {
      if (this.props.dataChat.status !== 'loading' && this.props.dataChat.status !== 'error') {
        this.handleSaveChat(this.props.dataChat.data, 'Bot')
      }
    }
  }

  handleSaveChat = (data, from) => {
    const { chatHistory } = this.state
    let temp = chatHistory
    temp.push({
      message: data,
      from: from
    })
    this.setState({
      chatHistory: temp
    })
  }
  render() {
    const { chatHistory } = this.state
    const { dataChat } = this.props
    return(
      <div className='chat-container'>
        <Row justify='center'>
          <Col span={20}>
            <Title>
              Example Chat
            </Title>
            <Row>
              <Col span={24}>
                <div className='card-chat'>
                  <div className='header-chat'>
                    <Text >
                      Help bot
                    </Text> 
                  </div>
                  <div className='body-chat'>
                    <ul className='container-massage'>
                      {chatHistory.length > 0 &&
                        chatHistory.map((data, index) => (
                        <li key={index} className={`${data.from === 'Bot' ? 'li_bot': 'li_user'}`}>
                          <div 
                            className={`${data.from === 'Bot' ? 'bot_name': 'user_name'}`} 
                            style={{width: `${data.message.length === 0 ? 10 : data.message.length}` * 9.2}}
                          >
                            <div className={`${data.from === 'Bot' ? 'bot-massage': 'user-massage'}`} >
                              <span className={`${data.from === 'Bot' ? 'header-chat-name': 'header-chat-name-user'}`}>{data.from}</span><br></br>
                              <Text>{data.message}</Text>
                            </div>
                          </div>
                        </li>
                        ))
                      }
                      {/* <li>
                        <div className='bot_name' style={{width: `${dataChat.data.length === 0 ? 10 : dataChat.data.length}` * 7.6}}>
                          <div className='bot-massage'>
                            <span className='header-chat-name'>Bot</span><br></br>
                            <Text>{dataChat.data}</Text>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div className='user_name'>
                          <div className='user-massage'>
                            <span className='header-chat-name-user'>You</span><br></br>
                            <Text>Oraiit</Text>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div className='bot_name'>
                          <div className='bot-massage'>
                            <span className='header-chat-name'>Bot</span><br></br>
                            <Text>{dataChat.data}</Text>
                          </div>
                        </div>
                      </li> */}
                    </ul>
                  </div>
                  <div className='footer-chat'>
                    <Input 
                      size='large'
                      value={this.state.inputKey}
                      onChange={(event) => this.handleKeyPrees(event)}
                      onPressEnter={() => this.handleOnClick()}
                      placeholder='Ketikan sesuatu...' 
                      addonAfter={<SendOutlined onClick={() => this.handleOnClick()}/>}
                    />
                  </div>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
        {/* <UIChat /> */}
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return{
    handleChatBot: (message) => dispatch(getChatbot.getChatBot(message))
  }
}

const mapStateToProps = (state) => {
  return {
    dataChat: state.chatbot
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ExampleChat)