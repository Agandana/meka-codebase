import React, { Component } from 'react'
import { connect } from 'react-redux'
import { UIForm } from '../../components'
import { Row, Col, Typography } from 'antd'
import './exampleForm.css'

const { Title } = Typography

class ExampleForm extends Component {

  handleFinish = val => {
    console.log('kirim data :', val)
  }

  render() {
    const LOV = [
      {
        id: '1',
        value: 'Pekalongan'
      },
      {
        id: '2',
        value: 'Jakarta'
      },
      {
        id: '3',
        value: 'Palu'
      },
      {
        id: '4',
        value: 'Ngawi'
      }
    ]
    const formItem = [
      {
        label: 'Username',
        name: 'username',
        maxLength: 20,
        type: 'input',
        colNumber: 12,
        rules: [
          {
            required: true, 
            message: 'Please input your username!' 
          }
        ],
        placeholder: 'Insert Username'
      },
      {
        label: 'Password',
        name: 'password',
        type: 'password',
        maxLength: 20,
        colNumber: 12,
        rules: [
          {
            required: true, 
            message: 'Please input your password!' 
          }
        ],
        placeholder: 'Insert Password'
      },
      {
        label: 'Postal Code',
        name: 'postalCode',
        maxLength: 5,
        type: 'input',
        colNumber: 12,
        rules: [
          {
            required: true, 
            message: 'Please input your Postal Code!' 
          }
        ],
        placeholder: 'Insert Postal Code'
      },
      {
        label: 'City',
        name: 'city',
        type: 'LOV',
        dataLOV: LOV,
        colNumber: 12,
        rules: [
          {
            required: true, 
            message: 'Please select your city!' 
          }
        ],
        placeholder: 'Insert City'
      },
      {
        label: 'Disabled',
        name: 'disabled',
        disabled: true,
        type: 'LOV',
        colNumber: 12,
        placeholder: 'Disabled Select'
      },
    ]
    return(
      <div className='form_body'>
        <Row justify='center'>
          <Col span={20}>
            <Title>Example Form</Title>
            <Row>
              <Col span={24}>
                <UIForm 
                  formItem={formItem} 
                  withRow={true}
                  finishForm={(val) => this.handleFinish(val)} 
                  buttonName={'Create'}
                  antdButton={true}
                  />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return{
    // handleDataHome: () => dispatch(getHome.getDataHome())
  }
}

const mapStateToProps = (state) => {
  return {
    // dataHome: state.home,
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ExampleForm)