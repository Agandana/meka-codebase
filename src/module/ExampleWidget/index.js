import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Row, Col, Typography } from 'antd'

const { Title } = Typography

class ExampleWidget extends Component {


  render() {
    return(
      <div className='form_body'>
        <Row justify='center'>
          <Col span={20}>
            <Title>Example Form</Title>
            <Row>
              <Col span={24}>
                <iframe
                    allow="microphone;"
                    width="350"
                    height="430"
                    src="https://console.dialogflow.com/api-client/demo/embedded/fd5301b1-678e-4096-8e15-69a1d16beb75">
                </iframe>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return{
    // handleDataHome: () => dispatch(getHome.getDataHome())
  }
}

const mapStateToProps = (state) => {
  return {
    // dataHome: state.home,
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ExampleWidget)