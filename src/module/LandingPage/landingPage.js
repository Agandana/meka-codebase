import React, { Component } from 'react'
import { connect } from 'react-redux'
import { UIForm } from '../../components'
import './landingpage.css'
import { FireFilled } from '@ant-design/icons';
import { Row, Col, Typography, Card, notification } from 'antd'

const { Title } = Typography

class LandingPage extends Component {
  constructor(props) {
    super(props)
    this.state = {

    }
  }

  handleFinish = val => {
    if (val.username === 'test' && val.password === 'test') {
      sessionStorage.setItem('login', true)
      this.props.onLogin()
      this.props.history.push('/dashboard');
    }else {
      notification.error({
        placement: 'bottomRight',
        message: 'Error',
        description: 'Username/Password Salah!'
      })
    }
  }

  render() {
    const formItem = [
      {
        label: 'Username',
        name: 'username',
        type: 'input',
        rules: [
          {
            required: true, 
            message: 'Please input your username!' 
          }
        ],
        placeholder: 'Username'
      },
      {
        label: 'Password',
        name: 'password',
        type: 'password',
        passowrdColor: '#EFEEEE',
        rules: [
          {
            required: true, 
            message: 'Please input your password!' 
          }
        ],
        placeholder: 'Password'
      }
    ]
    return(
      <div className="container_Body">
        <Row justify='center' className='body_Login'>
          <Col xs={12}>
            <Row gutter={[20,80]} justify='center'>
              <FireFilled className='icon_Fire'/>
            </Row>
            <Row gutter={[20,80]} justify='center'>
              <Title level={2} className='text_login'>Awesome Login!</Title>
            </Row>
            <Row gutter={[20,40]} justify='center' >
              <Col xs={16}>
                <Card className='card_login'>
                  <UIForm formItem={formItem} buttonClass="button-Form" finishForm={(val) => this.handleFinish(val)} buttonName={'Log In'}/>
                </Card>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return{
    // handleDataHome: () => dispatch(getHome.getDataHome())
  }
}

const mapStateToProps = (state) => {
  return {
    // dataHome: state.home,
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(LandingPage)