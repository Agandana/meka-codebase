import types from '../constants'
import chatbot from '../api/chatbot'

const getChatBot = (message) => dispatch => {
  dispatch({
    type: types.GET_CHATBOT_LOADING,
    payload: {
      status: 'loading',
      error: '',
      data: ''
    }
  })
  return chatbot.getChatBot(message).then(result => {
    if (result.status === 'error') {
      dispatch({
        type: types.GET_CHATBOT_FAILED,
        payload: result
      })
    }else {
      return dispatch({
        type: types.GET_CHATBOT_SUCCESS,
        payload: result
      })
    }
  })
}

export default {
  getChatBot
}