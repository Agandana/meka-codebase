import types from '../constants'
import home from '../api/home'

const getDataHome = () => dispatch => {
  dispatch({
    type: types.GET_HOME_LOADING,
    payload: {
      status: 'loading',
      error: '',
      data: []
    }
  })
  return home.getHome().then( result => {
    if (result.status === 'error') {
      dispatch({
        type: types.GET_HOME_FAILED,
        payload: result
      })
    }else {
      return dispatch({
        type: types.GET_HOME_SUCCESS,
        payload: result
      })
    }
  })
}

export default {
  getDataHome
}