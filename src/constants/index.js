import * as home from './home';
import * as chatbot from './chatbot'

export default {
    ...home,
    ...chatbot
}