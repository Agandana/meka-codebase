import types from '../constants'

const initialState = {
	status: 'loading',
	data: []
}

export default function home(state = initialState, action) {
	switch (action.type) {
		case types.GET_HOME_LOADING:
			return {
				...state,
				...action.payload
			}
		case types.GET_HOME_SUCCESS:
			return {
				...state,
				...action.payload
			}
		case types.GET_HOME_FAILED:
			return {
				...state,
				...action.payload
			}
		default:
			return {
				...state
			}
	}
}