import types from '../constants'

const initialState = {
	status: 'loading',
	data : '',
}

export default function chatbot(state = initialState, action) {
	switch (action.type) {
		case types.GET_CHATBOT_LOADING:
			return {
				...state,
				...action.payload
			}
		case types.GET_CHATBOT_SUCCESS:
			return {
				...state,
				...action.payload
			}
		case types.GET_CHATBOT_FAILED:
			return {
				...state,
				...action.payload
			}
		default:
			return {
				...state
			}
	}
}