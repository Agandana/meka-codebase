import { combineReducers } from 'redux'
import home from './home'
import chatbot from './chatbot'

export default combineReducers({
  home,
  chatbot
})